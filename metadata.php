<?php
/**
 * Metadata version
 */
$sMetadataVersion = '2.0';
/**
 * Module information
 */
$aModule = [
    'id'           => 'bnsales_solrsuche',
    'title'        => '<img src="../modules/bodynova/bnsales_solrsuche/out/img/favicon.ico" title="Bodynova Haendlershop Suche">odynova HaendlerShop Suche',
    'description'  => [
        'de' => 'Modul zur Bereitstellung der Solr Suche',
        'en' => 'Module for sales shop solr search'
    ],
    'thumbnail'     => 'out/img/logo_bodynova.png',
    'version'      => '2.0.0',
    'author'       => 'André Bender',
    'email'        => 'a.bender@bodynova.de',
    'controllers'  => [
        /* vorlage */
        /*
        'dre_user_rights'  =>
            \Bender\dre_AdminRights\Application\Controller\Admin\dre_user_rights::class
        */
        'dre_solrsuchecontroller'
        => \Bodynova\bnSales_Suche\Application\Controller\dre_SolrSuchController::class,

    ],
    'extend'       => [

        /* vorlage */
        /*
        \OxidEsales\Eshop\Application\Model\Article::class                      =>
            \Bender\dre_AdminRights\Application\Model\dre_article::class,
        */
        \OxidEsales\Eshop\Application\Model\Search::class                       =>
            \Bodynova\bnSales_Suche\Application\Model\dre_solrsuche::class,
        \OxidEsales\Eshop\Application\Model\ArticleList::class                  =>
            \Bodynova\bnSales_Suche\Application\Model\dre_ArticleList::class


    ],
    'templates' => [
        /* vorlage */
        /*
        'dre_user_rights.tpl'  =>
            'bender/dre_adminrights/Application/views/admin/tpl/dre_user_rights.tpl',
        */
        'dre_autocomplete.tpl' => 'bodynova/bnsales_solrsuche/Application/views/tpl/dre_autocomplete.tpl',
        /*
        'dre_autocomplete_bottom.tpl' => 'bender/dre_solrsuche/Application/views/tpl/dre_autocomplete_bottom.tpl',
        'dre_autocomplete_top.tpl' => 'bender/dre_solrsuche/Application/views/tpl/dre_autocomplete_top.tpl',*/
        'dre_searchresult.tpl' => 'bodynova/bnsales_solrsuche/Application/views/tpl/dre_searchresult.tpl',

    ],
    'settings' => [
        array(
            'group' => 'dre_solrSearch',
            'name' => 'blFeSearchEnabled',
            'type' => 'bool',
            'value' => true
        ),
        array(
            'group' => 'dre-search_main',
            'name' => 'dre-search-col',
            'type' => 'str',
            'value' => 'mycol1',
        ),
        array(
            'group' => 'dre-search_main',
            'name' => 'dre-search-url',
            'type' => 'str',
            'value' => 'https://sales.bodynova.com:8983/solr/',
        ),

    ],
    'events' => [
        /*
        'onActivate' =>
            'Bender\dre_AdminRights\Core\Events::onActivate'
        */
    ],
    'blocks'      => [
        array(
            'template' => 'layout/base.tpl',
            'block' => 'base_style',
            'file' => 'Application/views/blocks/dre_base_style.tpl'
        ),
        array(
            'template' => 'layout/base.tpl',
            'block' => 'base_js',
            'file' => 'Application/views/blocks/dre_base_js.tpl'
        ),
        array(
            'template' => 'widget/header/search.tpl',
            'block' => 'widget_header_search_form',
            'file' => 'Application/views/blocks/dre_solrsearch.tpl'
        ),
        /*array(
            'template' => 'widget/header/search_bottom.tpl',
            'block' => 'widget_header_search_form_bottom',
            'file' => 'Application/views/blocks/dre_solrsearch_bottom.tpl'
        ),
        array(
            'template' => 'widget/header/search_top.tpl',
            'block' => 'widget_header_search_form_top',
            'file' => 'Application/views/blocks/dre_solrsearch_top.tpl'
        ),*/
    ]
];
?>
