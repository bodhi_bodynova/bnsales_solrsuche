[{*$smarty.block.parent*}]

[{*assign var="sLanguage" value=$oView->getActiveLangAbbr()}]
[{capture name="myJS"}]
	$(document).ready(function () {
		var lang = '[{$sLanguage}]';
		//console.log('aktuelle Language:');
		//console.log(lang);
	});
[{/capture}]
[{oxscript add=$smarty.capture.myJS}]
[{oxscript include=$oViewConf->getModuleUrl('dre_solrsuche','out/src/js/dre_autocomplete.js')*}]

[{include file="dre_autocomplete.tpl"}]

[{if $oView->showSearch()}]
	<form class="form search" role="search" action="[{$oViewConf->getSelfActionLink()}]" method="get" name="search">
		[{$oViewConf->getHiddenSid()}]
		<input type="hidden" name="cl" value="search">
		<!-- Suchfeld -->
		[{block name="dd_widget_header_search_form_inner"}]
			<div class="input-group mb-3">
				[{block name="header_search_field"}]
					<input class="form-control" type="text" id="searchParam" name="searchparam" value="[{$oView->getSearchParamForHtml()}]" placeholder="[{oxmultilang ident="SEARCH"}]"
					       aria-label="[{oxmultilang ident="SEARCH"}]" aria-describedby="basic-addon2">
				[{/block}]

				[{block name="dd_header_search_button"}]
					<div class="input-group-append">
						<button class="btn btn-outline-primary" type="submit"
						        title="[{oxmultilang ident="SEARCH_SUBMIT"}]">
							<i class="fa fa-search"></i>
						</button>
					</div>
				[{/block}]
			</div>
		[{/block}]
	</form>
[{/if}]