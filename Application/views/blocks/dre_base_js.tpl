[{if !isset($oConfig)}]
	[{assign var="oConfig" value=$oViewConf->getConfig()}]
[{/if}]

[{if true == $oConfig->getConfigParam('blFeSearchEnabled')}]
	[{$smarty.block.parent}]
	[{oxscript include=$oViewConf->getModuleUrl('bnsales_solrsuche','out/src/js/accounting.js') priority=1}]
	[{oxscript include=$oViewConf->getModuleUrl('bnsales_solrsuche','out/src/js/jquery.easy-autocomplete.js') priority=1}]
	[{*oxscript include=$oViewConf->getModuleUrl('dre_solrsuche','out/src/js/dre_autocomplete.js')*}]
[{else}]
	[{$smarty.block.parent}]
[{/if}]


[{*}]
<script   src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script   src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
[{*}]






[{*oxscript include=$oViewConf->getModuleUrl('dre_solrsuche','out/src/js/d3_ext_search.js')*}]
[{*oxscript include=$oViewConf->getModuleUrl('dre_solrsuche','out/src/js/initall.js')*}]
[{*oxscript include=$oViewConf->getModuleUrl('dre_solrsuche','out/src/js/underscore.js')*}]
[{*oxscript include=$oViewConf->getModuleUrl('dre_solrsuche','out/src/js/underscore-min.js')*}]
[{*oxscript include=$oViewConf->getModuleUrl('dre_solrsuche','out/src/js/remarkable.js')*}]
[{*oxscript include=$oViewConf->getModuleUrl('dre_solrsuche','out/src/js/split.js')*}]
[{*oxscript include=$oViewConf->getModuleUrl('dre_solrsuche','out/src/js/FileSaver.min.js')*}]
[{*oxscript include=$oViewConf->getModuleUrl('dre_solrsuche','out/src/js/dre_autocomplete.js')*}]




[{*}] Vorlage von ddoevisualcms
[{assign var="oConf" value=$oView->getConfig()}]

[{if !$oViewConf->isRoxiveTheme()}]
    [{if !$oConf->getConfigParam('blDisableJQuery')}]
        [{oxscript include=$oViewConf->getModuleUrl('ddoevisualcms','out/src/js/jquery.min.js') priority=1}]
        [{oxscript include=$oViewConf->getModuleUrl('ddoevisualcms','out/src/js/jquery-ui.min.js') priority=1}]
    [{/if}]
    [{if !$oConf->getConfigParam( 'blCustomGridFramework' ) && !$oConf->getConfigParam('blDisableBootstrap')}]
        [{oxscript include=$oViewConf->getModuleUrl('ddoevisualcms','out/src/js/bootstrap-custom.min.js') priority=1}]
    [{/if}]
[{/if}]

[{oxscript include=$oViewConf->getModuleUrl('ddoevisualcms','out/src/js/photoswipe.min.js')}]
[{oxscript include=$oViewConf->getModuleUrl('ddoevisualcms','out/src/js/scripts.min.js')}]

[{include file="ddoevisualcms_photoswipe.tpl"}]
[{*}]
