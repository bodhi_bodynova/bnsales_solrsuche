[{capture name="myJS"}]
	$(document).ready(function () {
		var options = {
			/**
			 * auswertung der Usereingabe und absetzen des Http_GET Request an form.php
			 * zuvor validierung der Sprachauswahl de als Standarteinstellung berücksichtigt.
			 */
			url: function(phrase) {
				return "[{$oViewConf->getBaseDir()}]index.php?cl=dre_solrsuchecontroller&fnc=suche&phrase=" + phrase;
			},
			ajaxSettings: {
				dataType: "json"
			},
			listLocation: function (response) {
				return response.artikelobjekte; //response.response.docs
			},
			theme: "solid",
			/**
			 * Value des Search Fields bis eingabe vorgenommen wird
			 */
			placeholder: "[{oxmultilang ident='SUCHBEGRIFF'}]", //"",
			/**
			 * Value des Search Fields, wenn eine Auswahl getroffen wurde
			 */
			getValue: function(response){
				return response.suchwort;
			},
			/**
			 * Einstellung: Wo der Suchbegriff mit dem Ergebnis matched, wird dies hervorgehoben
			 */
			highlightPhrase: true,
			/**
			 * Zeitverzögerung bis die Abfrage abgeschickt wird bei Usereingabe
			 */
			requestDelay: 400,
			/**
			 * Anzahl der Mindestzeichen bis die Suche ausgelöst wird
			 */
			minCharNumber: 3,
			/**
			 * Objekt list Array von Objekten für die Auswahlliste
			 */
			list:{
				maxNumberOfElements: 30,
				/**
				 * Objekt für die Animation bei der Darstellung der Liste
				 * */
				showAnimation: {
					type: "fade", //normal|slide|fade
					time: 200,
					callback: function(response) {
						//console.log(response);
					}
				},
				/**
				 * Trigger der ausgelöst wird, wenn die Liste geladen wird
				 * */
				onLoadEvent:function(response){
					$('#eac-container-searchParam > ul');
					//console.log(listLocation);
				},
				/**
				 *
				 * Trigger der ausgelöst wird, wenn der User eine Auswahl getroffen hat
				 *
				 * */
				onClickEvent: function () {
				}
			},
			/**
			 *
			 * Template Objekt als Array von Objekten
			 *
			 * */
			template: {
				type: "custom",
				method: function(value, item){
					//console.log('item');
					//console.log(item);
					var price = null;
					var formprice = 0;
					var oxid = item.oxarticles__oxid.value;
					var titel = item.oxarticles__oxtitle.value;


					if(parseInt(item.preis) > 0){
						formprice = accounting.formatMoney(item.preis, "€", 2, ".", ",","%v %s");
					}else{
						formprice = 99;//accounting.formatMoney(item.oxarticles__oxprice.value, "€", 2, ".", ",","%v %s");
					}

					var string = "<a onclick=\"jumpToArticle('"+ oxid +"','" + oxid + "','"+[{$oViewConf->getActLanguageID()}]+"');\">"+
									"<div style='text-align: left'><img src='" + item.pic + "' width='50px' height='50px' style='float:left;'>"+
									"<span class='ArtTitle pull-left'>" + titel + "</span>" +
									/*"<br/><span class='small' style='color:#777;'>[{oxmultilang ident="PRODUCT_NO" suffix="COLON"}] " + item.artnum + "</span>"+*/
									"<span class='pull-right' style='font-family: Raleway,\"Helvetica Neue\",Helvetica,Arial,sans-serif'>" + formprice + "</span><br/>"+
									"<span style='word-wrap: break-word; white-space: break-spaces; font-size: 75%; font-weight: normal'>"+
									item.children +"</span>"+
									"</div>"+
						"</a>";
					return string;
				}
			}
		};
		$("#searchParam").easyAutocomplete(options);
	});
[{/capture}]
[{oxscript add=$smarty.capture.myJS}]