<?php

namespace Bodynova\bnSales_Suche\Application\Controller;


use OxidEsales\Eshop\Core\Registry;

class dre_SolrSuchController extends \OxidEsales\Eshop\Application\Controller\FrontendController
{

    protected $_sClass = 'dre_solrsuchecontroller';
    /**
     * Current class template name.
     *
     * @var string
     */
    protected $_sThisTemplate = 'dre_searchresult.tpl';

    protected $suchurl = '';
    /**
     * @return null
     */
    public function render(){
        parent::render();
        return $this->_sThisTemplate;
    }

    public function suche(){
        $searchurl = Registry::getConfig()->getConfigParam('dre-search-url');
        $searchcol = Registry::getConfig()->getConfigParam('dre-search-col');
        $this->suchurl = $searchurl . $searchcol . '/select';

        $explodiert = explode(' ', str_replace('%20', ' ', $_GET['phrase']));
        $searchkey = implode(' ', $explodiert);

        // suche nach artikelnummer und ausgabe bei einem einzigen Ergebnis.
        $suchresult = $this->getSearchArtnum($searchkey);
        $sucherg = json_decode($suchresult);

        $arrRueckgabe['suchwort'] = $searchkey;


        // bei einem eindeutigen Ergebnis nur dies darstellen.
        if($this->getSearchCount($sucherg) == 1){
            $result = $this->verarbeiteErgebnis($sucherg);
            #$result['suchwort'] = $arrRueckgabe['suchwort'];
            echo json_encode($result);
            die();
        }

        // wenn keine Eindeutigkeit gefunden wurde add fuzzy und leventstein
        $searchkey = implode('~ ', $explodiert) . '~';

        $suchresult = $this->getSearchResults($searchkey);
        $sucherg = json_decode($suchresult);

        // wenn nix gefunden wurde weitere Verarbeitung abbrechen und log in Datei
        $arrRueckgabe['anzahlGefunden'] = $this->getSearchCount($sucherg);

        if ($this->getSearchCount($sucherg) == 0) {
            $file = fopen(dirname(__DIR__)."/nixgefunden.txt", "a+");
            fwrite($file, $searchkey."\n");
            fclose($file);
            json_encode($arrRueckgabe);
            die();
        }

        if ($this->getSearchCount($sucherg) > 1) {
            $result = $this->verarbeiteErgebnis($sucherg);
            echo json_encode($result);
            die();
        }



        /*
        $phrase = $_GET['phrase'];

        $arrArtikelIds = array();
        foreach ($sucherg->response->docs as $artikel){
            $arrArtikelIds[] = $artikel->id;
        }
        $arrRueckgabe['artikelids'] = $arrArtikelIds;
        foreach ($arrArtikelIds as $artikelid){
            $articleobj = oxNew(\OxidEsales\Eshop\Application\Model\Article::class);
            $transarticle = $articleobj;
            //$transarticle->enablePriceLoad();
            $transarticle->suchwort = $phrase;

            $transarticle->load($artikelid);
            if($transarticle->isVariant()){
                $transarticle->getParentArticle();
            }
            if($transarticle->__get('oxarticles__oxactive')->rawValue == 1){
                //$transarticle->enablePriceLoad();
                $transarticle->artnum = $transarticle->__get('oxarticles__oxartnum')->rawValue;
                $transarticle->user = $transarticle->getArticleUser();
                $transarticle->getVarMinPrice();
                $transarticle->getTPrice();
                $transarticle->preis = $transarticle->getPrice();
                $link = $transarticle->getLink();
                $transarticle->pic = $transarticle->getIconUrl();
                $transarticle->link = $link;
                $transarticle->children = $transarticle->__get('oxarticles__childartnums')->rawValue;
                $arrRueckgabe['artikelobjekte'][] = $transarticle;
            }
        }

        echo json_encode($arrRueckgabe);
        */
    }

    public function getSearchResults($searchkey){
        return file_get_contents($this->suchurl.'?q=' . urlencode($searchkey).  '&indent=on&wt=json');
    }

    public function getSearchArtnum($searchkey){
        stream_context_set_default(array(
            'ssl' => array(
                'peer_name' => 'generic-server',
                'verify_peer' => FALSE,
                'verify_peer_name' => FALSE,
                'allow_self_signed' => TRUE
            )));
        $result = null;
        try{
            $result = file_get_contents($this->suchurl . '?q="' . urlencode($searchkey) . '"&indent=on&wt=json');
        }catch(\Exception $e){
            echo '<pre>';
            print_r($e);
            echo '</pre>';
            die();
        }
        return $result;
    }

    public function getSearchCount($sucherg){
        return $sucherg->response->numFound;
    }

    public function test(){
        $phrase = $_GET['phrase'];
        echo file_get_contents($this->suchurl."?q=" . $phrase.  "&indent=on&wt=json");
    }
    public function getArticleById($oxid){
        $articleobj = oxNew(\OxidEsales\Eshop\Application\Model\Article::class);
        return $articleobj->load($oxid);
    }

    public function verarbeiteErgebnis($sucherg){
        $phrase = $_GET['phrase'];

        $arrArtikelIds = array();
        foreach ($sucherg->response->docs as $artikel) {
            $arrArtikelIds[] = $artikel->id;
        }

        $arrRueckgabe['artikelids'] = $arrArtikelIds;
        foreach ($arrArtikelIds as $artikelid) {
            $transarticle = oxNew(\OxidEsales\Eshop\Application\Model\Article::class);
            $transarticle->enablePriceLoad();
            $transarticle->load($artikelid);

            if ($transarticle->isVariant()) {
                $transarticle->getParentArticle();
            }elseif($transarticle->hasMdVariants()){
                $transarticle->preis = $transarticle->getVarMinPrice()->getPrice();
            }else{
                $transarticle->preis = $transarticle->getPrice()->getPrice();
            }


            if ($transarticle->oxarticles__oxactive->value == 1) {
                $transarticle->suchwort = $phrase;
                //$transarticle->enablePriceLoad();
                //$transarticle->user = $transarticle->getArticleUser();
                #$transarticle->artnum = $transarticle->__get('oxarticles__oxartnum')->rawValue;
                #$transarticle->getVarMinPrice();
                #$transarticle->getTPrice();
                //$transarticle->preis = $transarticle->getPreisAktuell();
                //$link = $transarticle->getLink();
                $transarticle->pic = $transarticle->getIconUrl();
                //$transarticle->link = $link;
                $transarticle->children = $transarticle->__get('oxarticles__childartnums')->rawValue;
                $arrRueckgabe['artikelobjekte'][] = $transarticle;
            }

            #d($arrRueckgabe);


            #d($transarticle);
            #die();
        }
        return $arrRueckgabe;
    }
}