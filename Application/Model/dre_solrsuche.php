<?php

namespace Bodynova\bnSales_Suche\Application\Model;

use OxidEsales\Eshop\Core\Registry;

class dre_solrsuche extends dre_solrsuche_parent {

    /**
     * Hole die Suchurl und den eingestellten Knoten aus dem Shop-Backend
    */
    public function getSearchUrl(){
        $searchurl = Registry::getConfig()->getConfigParam('dre-search-url');
        $searchcol = Registry::getConfig()->getConfigParam('dre-search-col');
        return $searchurl . $searchcol . '/select';
    }

    /**
     * Suche für genaue Ergebnisse wie Artikelnummern
     */
    public function solrsuche($searchkey)
    {
        $suchresult = file_get_contents($this->getSearchUrl() . '?q=' . urlencode($searchkey) . '&indent=on&wt=json');
        $sucherg = json_decode($suchresult);
        $arrArtikelIds = array();
        foreach ($sucherg->response->docs as $artikel) {
            $arrArtikelIds[] = $artikel->id;
        }
        return $arrArtikelIds;
    }

    /**
     * Suche mit erhöhter Fehlertoleranz
     */
    public function solrfuzzysuche($searchkey)
    {
        $suchresult = file_get_contents($this->getSearchUrl() . '?q=' . urlencode($searchkey) . '~&indent=on&wt=json');
        $sucherg = json_decode($suchresult);
        $arrArtikelIds = array();
        foreach ($sucherg->response->docs as $artikel) {
            $arrArtikelIds[] = $artikel->id;
        }
        return $arrArtikelIds;
    }

    /**
     * Shop eigene SQL Suche um Solr Eigenschaften erweitert
     */
    public function getSearchArticles($sSearchParamForQuery = false, $sInitialSearchCat = false, $sInitialSearchVendor = false, $sInitialSearchManufacturer = false, $sSortBy = false)
    {
        // sets active page
        $this->iActPage = (int)\OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter('pgNr');
        $this->iActPage = ($this->iActPage < 0) ? 0 : $this->iActPage;

        $iNrofCatArticles = $this->getConfig()->getConfigParam('iNrofCatArticles');
        $iNrofCatArticles = $iNrofCatArticles ? $iNrofCatArticles : 10;

        $oArtList = null;
        $arrArtikelIds = $this->solrsuche($sSearchParamForQuery);
        if(count($arrArtikelIds) == 0){
            $arrArtikelIds = $this->solrfuzzysuche($sSearchParamForQuery);
        }

        $oArtList = oxNew(\OxidEsales\Eshop\Application\Model\ArticleList::class);
        $oArtList->loadIdsSort($arrArtikelIds, $arrArtikelIds);
        return $oArtList;
    }

    /**
     * Anzeige der gefunden Treffer auf der Ergebnisseite
    */
    public function getSearchArticleCount($sSearchParamForQuery = false, $sInitialSearchCat = false, $sInitialSearchVendor = false, $sInitialSearchManufacturer = false)
    {
        $iCnt = 0;
        $arrArtikelIds = $this->solrsuche($sSearchParamForQuery);
        if (count($arrArtikelIds) == 0) {
            $arrArtikelIds = $this->solrfuzzysuche($sSearchParamForQuery);
        }
        $iCnt = count($arrArtikelIds);
        return $iCnt;
    }

    public function getSelectStatement($sSearchParamForQuery, $strFields = null)
    {
        $search = $sSearchParamForQuery;
        $return = "SELECT * FROM oxarticles as a WHERE a.OXACTIVE = 1 AND a.OXPARENTID = '' AND (a.oxid IN(SELECT oxid FROM oxarticles WHERE oxtitle LIKE '%" . $search . "%' OR  oxtitle_1 LIKE '%" . $search . "%' OR oxtitle_2 LIKE '%" . $search . "%' OR oxartnum LIKE '%" . $search . "%' OR oxsearchkeys LIKE '%" . $search . "%' OR oxsearchkeys_1 LIKE '%" . $search . "%' OR oxsearchkeys_2 LIKE '%" . $search . "%') OR a.oxid IN(SELECT oxparentid FROM oxarticles WHERE oxtitle LIKE '%" . $search . "%' OR  oxtitle_1 LIKE '%" . $search . "%' OR oxtitle_2 LIKE '%" . $search . "%' OR oxartnum LIKE '%" . $search . "%' OR oxsearchkeys LIKE '%" . $search . "%' OR oxsearchkeys_1 LIKE '%" . $search . "%' OR oxsearchkeys_2 LIKE '%" . $search . "%'))";
        return $return;
    }

    /**
     * @return string
     */
    public function getAutosuggestSelectColumns()
    {
        //
        $strColumns = ' a.OXPARENTID, a.OXID, ';
        $strColumns .= ' a.OXARTNUM, a.OXSEARCHKEYS, OXARTNUM, OXPRICE, OXPRICEA, OXPRICEB, OXPRICEC, OXPRICED, OXPRICEE, OXPIC1, bnflagbestand, OXDELIVERY,';
        //
        $strColumns .= 'IF(a.OXPARENTID!=\'\', (SELECT oxsub.OXTITLE' . ($this->_iLanguage > 0 ? '_' . $this->_iLanguage : '') . ' FROM oxarticles AS oxsub WHERE oxsub.oxid=a.OXPARENTID), \'\') AS parenttitle, ';
        //
        $strColumns .= 'IF(a.OXPARENTID!=\'\', a.OXVARSELECT' . ($this->_iLanguage > 0 ? '_' . $this->_iLanguage : '') . ', a.OXTITLE' . ($this->_iLanguage > 0 ? '_' . $this->_iLanguage : '') . ') AS OXTITLE';
        //
        return $strColumns;
    }


    /**
     *
     */
    public function getFaultCorrectedSearch($strSearchedString)
    {
        $objDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb(\OxidEsales\Eshop\Core\DatabaseProvider::FETCH_MODE_ASSOC);
        //
        $arrWords = explode(' ', $strSearchedString);
        //
        $arrNewWords = array();
        //
        foreach ($arrWords AS $strWord) {
            $strWordSoundexed = soundex($strWord) . '<br>';
            //
            //$arrRes = $objDb->getAll('SELECT wort FROM oxsearchablewords WHERE soundexchar=' . $objDb->quote(substr($strWordSoundexed, 0, 1)) . ' AND soundexval=' . $objDb->quote(substr($strWordSoundexed, 1)) . ' AND lang=\'' . \OxidEsales\Eshop\Core\Registry::getLang()->getBaseLanguage() . '\' ORDER BY aufkommen DESC LIMIT 1');
            $arrRes = $objDb->getAll('SELECT wort FROM oxsearchablewords WHERE soundexchar=' . $objDb->quote(substr($strWordSoundexed, 0, 1)) . ' AND soundexval=' . $objDb->quote(substr($strWordSoundexed, 1)) . ' AND lang=\'' . \OxidEsales\Eshop\Core\Registry::getLang()->getBaseLanguage() . '\' ORDER BY aufkommen DESC LIMIT 1');


            //
            if (isset($arrRes[0])) {
                $arrNewWords[] = array('changed' => (strtolower($strWord) != $arrRes[0]['wort']), 'word' => $arrRes[0]['wort']);
            } else {
                $arrNewWords[] = array('changed' => false, 'word' => $strWord);
            }
        }
        return $arrNewWords;
    }

}