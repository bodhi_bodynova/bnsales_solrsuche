<?php
namespace Bodynova\bnSales_Suche\Application\Model;

class dre_ArticleList extends \OxidEsales\Eshop\Application\Model\ArticleList{

    /**
     * Load the list by article ids
     *
     * @param array $aIds Article ID array
     *
     * @return null;
     */
    public function loadIdsSort($aIds, $sort = null)
    {
        if (!count($aIds)) {
            $this->clear();

            return;
        }

        $oBaseObject = $this->getBaseObject();
        $sArticleTable = $oBaseObject->getViewName();
        $sArticleFields = $oBaseObject->getSelectFields();

        $oxIdsSql = implode(',', \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->quoteArray($aIds));

        $sSelect = "select $sArticleFields from $sArticleTable ";
        $sSelect .= "where $sArticleTable.oxid in ( " . $oxIdsSql . " ) and ";
        $sSelect .= $oBaseObject->getSqlActiveSnippet();
        if($sort){
            $sSelect .= " Order By FIELD( $sArticleTable.oxid, '" . implode('\' ,\'', $sort) . "')";
        }

        #echo $sSelect;
        #die();

        $this->selectString($sSelect);
    }

    public function loadByArticlesIDs($arrOXIDs)
    {
        //
        $sArticleTable = getViewName('oxarticles');
        //
        $sSelect  = "select * from $sArticleTable ";
        $sSelect .= "where ".$this->getBaseObject()->getSqlActiveSnippet()." and $sArticleTable.OXID IN('".implode("','", $arrOXIDs)."') ";
        #$sSelect .= "order by $sArticleTable.OXTITLE_1 desc";
        //

        $this->selectString($sSelect);
        // ende
    }
}